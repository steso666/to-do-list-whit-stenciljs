# todo-app



<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [my-list](../my-list)

### Depends on

- [todo-list-form](../todo-list-form)
- [todo-list](../todo-list)

### Graph
```mermaid
graph TD;
  todo-app --> todo-list-form
  todo-app --> todo-list
  my-list --> todo-app
  style todo-app fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
