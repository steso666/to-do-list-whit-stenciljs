import { newSpecPage } from '@stencil/core/testing';
import { TodoList } from '../../todo-list/todo-list';
import { TodoApp } from '../todo-app';

describe('todo-app', () => {
  it('should builds', () => {
    expect(new TodoApp()).toBeTruthy();
  });
});

describe('todo-list', () => {
  it('should builds', () => {
    expect(new TodoList()).toBeTruthy();
  });
});

describe('todo-app', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [TodoApp],
      html: `<todo-app></todo-app>`,
    });
    expect(page.root).toEqualHtml(`
      <todo-app>
        <div>
        <todo-list-form></todo-list-form>
        <div class="table-scroll table-scroll-width-none"><todo-list/></div>
        </div>
      </todo-app>
    `);
  });
});

describe('when clicking', () => {
  let page;
  let pageRoot;
  beforeEach(async () => {
    page = await newSpecPage({
      components: [TodoApp, TodoList],
      html: `<todo-app></todo-app>`
    });
    pageRoot = page.root;
  });

  it('should be able to set completed class', async () => {
    let task = { completed: false }
    const event = new CustomEvent('task-completed', { detail: task, bubbles: true });
    pageRoot.querySelector('todo-list').dispatchEvent(event);
    expect(task).toEqual({ completed: true });
  });
});
