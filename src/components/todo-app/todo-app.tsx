import { Component, State, h, Listen } from '@stencil/core';
import { TodoItem } from "../../models/todoItem";

@Component({
  tag: 'todo-app',
  styleUrl: 'todo-app.css',
})

export class TodoApp {
  @State() items: TodoItem[] = [];
  @State() task: TodoItem = null;
  @State() editing: boolean = false;

  @Listen('add-item')
  addItem(event: CustomEvent<TodoItem>) {
    this.items = [...this.items, event.detail]
  }

  @Listen('delete')
  delete(event: CustomEvent<TodoItem>) {
    const taskIndex = this.items.indexOf(event.detail);
    if (taskIndex > -1) {
      this.items.splice(taskIndex, 1);
      this.items = [...this.items];
    }
  }

  @Listen('task-completed')
  taskComplet(event: CustomEvent<TodoItem>) {
    const task = event.detail;
    task.completed = !task.completed;
    this.items = [...this.items];
  }

  @Listen('edit-task')
  edit(event: CustomEvent<TodoItem>){
    this.task = event.detail;
    this.editing = true;

  }

  @Listen('save-edit')
  saveEdit(){   
    this.items = [...this.items];
    this.task = null; 
    this.editing = false;   
  }
  
  render() {
    return (
      <div>
        <todo-list-form item={this.task}/>
        <div class="table-scroll table-scroll-width-none"><todo-list todoItems={this.items} disabledDelete={this.editing} /></div>
      </div>
    );
  }
}
