import { Component, Prop, h, Event, EventEmitter, Element } from '@stencil/core';
import { TodoItem } from "../../models/todoItem";

@Component({
  tag: 'todo-list',
  styleUrl: 'todo-list.css',
  shadow: false
})

export class TodoList {
  @Prop() todoItems: TodoItem[] = [];
  @Element() element: HTMLElement;
  @Prop({ mutable: true }) disabledDelete: boolean;
  @Event({
    eventName: 'delete',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) delete: EventEmitter<TodoItem>;

  @Event({
    eventName: 'task-completed',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) taskComplet: EventEmitter<TodoItem>;

  @Event({
    eventName: 'edit-task',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) edit: EventEmitter<TodoItem>;

  taskCompleted(item: TodoItem) {
    this.taskComplet.emit(item);
  }

  deleteTask = (item: TodoItem) => {
    this.delete.emit(item);
  }

  editTask = (item: TodoItem) => {
    this.edit.emit(item);
  }

  render() {
    return (
      <div class="tasks-container">
        {
          this.todoItems.map((item: TodoItem) => (
            <div class="task">
              <span class={item.completed ? 'completed' : ''} onClick={() => this.editTask(item)}>{item.text}</span>
              <button class="btn done" onClick={() => this.taskCompleted(item)}><i class={item.completed ? 'fas fa-ban' : 'fas fa-check-circle'}></i></button>
              <button disabled={this.disabledDelete} class={this.disabledDelete ? 'btn disabled' : 'btn delete'} onClick={() => this.deleteTask(item)}><i class="far fa-trash-alt"></i></button>
            </div>
          ))
        }
      </div>
    );
  }
}
