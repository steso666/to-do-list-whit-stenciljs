# todo-list



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute | Description | Type         | Default |
| ----------- | --------- | ----------- | ------------ | ------- |
| `todoItems` | --        |             | `TodoItem[]` | `[]`    |


## Events

| Event            | Description | Type                    |
| ---------------- | ----------- | ----------------------- |
| `delete`         |             | `CustomEvent<TodoItem>` |
| `task-completed` |             | `CustomEvent<TodoItem>` |


## Dependencies

### Used by

 - [todo-app](../todo-app)

### Graph
```mermaid
graph TD;
  todo-app --> todo-list
  style todo-list fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
