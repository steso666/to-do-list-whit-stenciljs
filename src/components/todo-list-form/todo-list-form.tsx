import { Component, Event, EventEmitter, State, h, Prop, Watch } from '@stencil/core';
import { TodoItem } from "../../models/todoItem";


@Component({
  tag: 'todo-list-form',
  styleUrl: 'todo-list-form.css'
})
export class TodoListForm {

  @State() userInput: string = '';
  @Prop() item: TodoItem = null;
  @Watch('item') itemChange() {
    if (this.item) {
      this.userInput = this.item.text;
    }
  }

  @Event({
    eventName: 'add-item',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) addItem: EventEmitter<TodoItem>;

  @Event({
    eventName: 'save-edit',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) saveEdit: EventEmitter;

  handleOnInput(event) {
    this.userInput = event.target.value;
  }

  handleOnSubmit(event) {
    event.preventDefault();
    if (!this.userInput.trim()) {
      alert("Please enter a task to be done");
      return;
    }
    if (this.item) {
      this.item.text = this.userInput;
      this.saveEdit.emit();
    } else {
      this.addItem.emit(new TodoItem(this.userInput, false));
    }
    this.userInput = '';
  }

  render() {
    return (
      <form onSubmit={this.handleOnSubmit.bind(this)}>
        <div id="newtask">
          <input type="text" placeholder="Task to be done.." onInput={this.handleOnInput.bind(this)} value={this.item ? this.item.text : this.userInput} />
          <button class={this.item ? 'edit-task' : 'add-task'}>{this.item ? 'Edit task' : 'Add task'}</button>
        </div>
      </form>
    );
  }
}
