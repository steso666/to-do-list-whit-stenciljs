import { newSpecPage } from '@stencil/core/testing';
import { TodoListForm } from '../todo-list-form';

describe('todo-list-form', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [TodoListForm],
      html: `<todo-list-form></todo-list-form>`,
    });
    expect(page.root).toEqualHtml(`
      <todo-list-form>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </todo-list-form>
    `);
  });
});
