import { newE2EPage } from '@stencil/core/testing';

describe('todo-list-form', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<todo-list-form></todo-list-form>');

    const element = await page.find('todo-list-form');
    expect(element).toHaveClass('hydrated');
  });
});
