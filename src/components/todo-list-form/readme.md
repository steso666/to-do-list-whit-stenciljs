# todo-list-form



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type         | Default |
| -------- | --------- | ----------- | ------------ | ------- |
| `items`  | --        |             | `TodoItem[]` | `[]`    |


## Events

| Event      | Description | Type                    |
| ---------- | ----------- | ----------------------- |
| `add-item` |             | `CustomEvent<TodoItem>` |


## Dependencies

### Used by

 - [todo-app](../todo-app)

### Graph
```mermaid
graph TD;
  todo-app --> todo-list-form
  style todo-list-form fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
