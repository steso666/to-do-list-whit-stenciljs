import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'my-list',
  styleUrl: 'my-list.css',

})
export class MyList {
  @Prop() name: string = "Todo List with StencilJS";

  render() {
    return (
      <div class="container">
        <h1>{this.name}</h1>
        <main>
          <todo-app />
        </main>
      </div>
    );
  }
}