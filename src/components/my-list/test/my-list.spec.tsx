import { newSpecPage } from '@stencil/core/testing';
import { MyList } from '../my-list';

describe('my-list', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [MyList],
      html: `<my-list></my-list>`,
    });
    expect(page.root).toEqualHtml(`
      <my-list>
      <div class="container">
      <h1>Todo List with StencilJS</h1>
      <main>
        <todo-app />
      </main>
    </div>
      </my-list>
    `);
  });
});
