# my-list



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type     | Default                      |
| -------- | --------- | ----------- | -------- | ---------------------------- |
| `name`   | `name`    |             | `string` | `"Todo List with StencilJS"` |


## Dependencies

### Depends on

- [todo-app](../todo-app)

### Graph
```mermaid
graph TD;
  my-list --> todo-app
  todo-app --> todo-list-form
  todo-app --> todo-list
  style my-list fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
